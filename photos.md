[![Aaron Swartz](Aaron_Swartz_at_Boston_Wikipedia_Meetup,_2009-08-18.jpg)](https://commons.wikimedia.org/w/index.php?search=aaron+swartz&title=Special%3ASearch&go=Go&ns0=1&ns6=1&ns12=1&ns14=1&ns100=1&ns106=1#/media/File:Aaron_Swartz_at_Boston_Wikipedia_Meetup,_2009-08-18.jpg)

By Sage Ross - Flickr: Boston Wiki Meetup, CC BY-SA 2.0, https://commons.wikimedia.org/w/index.php?curid=15852722

[![Alexandra Elbakian](Alexandra_Elbakyan.jpg)](https://fr.wikipedia.org/wiki/Sci-Hub#/media/File:Alexandra_Elbakyan.jpg)

Par Apneet Jolly — https://www.flickr.com/photos/ajolly/4696604402/, [CC BY 2.0](https://commons.wikimedia.org/w/index.php?curid=47280109)

[![Julian Assange](RUEDA_DE_PRENSA_CONJUNTA_ENTRE_CANCILLER_RICARDO_PATIÑO_Y_JULIAN_ASSANGE_(cropped).jpg)](https://commons.wikimedia.org/wiki/Category:Julian_Assange_in_2014#/media/File:RUEDA_DE_PRENSA_CONJUNTA_ENTRE_CANCILLER_RICARDO_PATI%C3%91O_Y_JULIAN_ASSANGE_(cropped).jpg)

By David G. Silvers, Cancillería del Ecuador - https://www.flickr.com/photos/dgcomsoc/14933990406/, CC BY-SA 2.0, https://commons.wikimedia.org/w/index.php?curid=77947969

[![Chelsea Manning](Chelsea_Manning_on_18_May_2017.jpg)](https://commons.wikimedia.org/wiki/Category:Chelsea_Manning#/media/File:Chelsea_Manning_on_18_May_2017.jpg){ width=50% }

By Tim Travers Hawkins, CC BY-SA 4.0
https://commons.wikimedia.org/w/index.php?curid=59051881

[![Edward Snowden](Edward_Snowden-2.jpg)](https://commons.wikimedia.org/wiki/Category:Edward_Snowden_in_2013#/media/File:Edward_Snowden-2.jpg)

By Laura Poitras / Praxis Films, CC BY 3.0, https://commons.wikimedia.org/w/index.php?curid=27176492
