
---
lang: fr
papersize: a4
geometry: margin=1.25in
fontsize: 12pt
fontfamily: quattrocento
linestretch: 1.25
...


## Alexandra Elbakian

**Alexandra Elbakyan** commence à travailler dans la sécurité informatique à Moscou. En 2010, elle se rend à l'Université de Fribourg-en-Brisgau, en Allemagne, pour étudier les neurosciences. Son projet de recherche concerne l'interactivité cerveau-machine4. Elle observe une forte demande pour des articles seulement accessibles sur péage pour des montants qu'elle juge excessifs, chaque article coûtant environ 30 $. Elle décide en 2011 de créer un système qui permettrait d'avoir en accès libre des publications universitaires grâce à des téléchargements de personnes abonnées aux sites à péage, le futur Sci-Hub.

Elle fait un stage d'été au Georgia Institute of Technology puis revient étudier au Kazakhstan dans le domaine de l'histoire des sciences.

En octobre 2015, un tribunal fédéral de New York la déclare coupable d'avoir piraté des articles scientifiques de l'éditeur Elsevier. Afin d'éviter d'être extradée, elle met en pause ses études en neurosciences pour suivre un programme d'histoire des arts dans une petite université qui n'a pas été dévoilée. Ses études se portent alors sur la communication scientifique.

Le journal The New York Times l'a comparée à Edward Snowden à cause de son désir de propager massivement de l'information et parce qu'elle réside en Russie, où le système judiciaire américain ne peut intervenir. Le site Ars Technica la compare à Aaron Swartz. Ses efforts pour rendre des millions de documents accessibles au plus grand nombre sans respecter le droit d'auteur lui ont valu le surnom de « Robin des Bois de la science ». En décembre 2016, la revue Nature la place dans son classement des dix personnes les plus influentes en sciences de l'année écoulée.

_[Source : wikipedia](https://fr.wikipedia.org/wiki/Alexandra_Elbakyan)- CC-By-SA _

\newpage

## Chelsea Manning

**Chelsea Elizabeth Manning**, née Bradley Edward Manning le 17 décembre 1987 à Crescent (Oklahoma), est une ancienne analyste militaire de l'armée des États-Unis de nationalité américano-britannique qui a été condamnée et incarcérée pour trahison.

Manning transmet en 2010 à WikiLeaks des documents militaires classés secret défense relevant du domaine de la Défense Nationale, notamment sur la mort de civils pendant la guerre d'Afghanistan (Afghan War Diary) et des documentations visuelles de bavures de l'U.S. Army pendant la guerre d'Irak (photos de l'humiliation de détenus de la prison d'Abou Ghraib, vidéo du raid aérien du 12 juillet 2007 à Bagdad). La diffusion de ces informations lui vaut d'être condamné le 21 août 2013 à trente-cinq ans de prison.

Au lendemain de sa condamnation, Manning déclare être une personne transgenre et entame des démarches pour changer d'identité et prendre le prénom de Chelsea. Le 23 avril 2014, la justice américaine reconnaît le changement de nom de Manning, qui s'appelle désormais officiellement Chelsea Elizabeth Manning. En février 2015, l'armée autorise Manning à entamer son traitement hormonal, et le mois suivant, la Cour d'appel de l'U.S. Army statue que Chelsea Manning doit être désignée via des pronoms féminins ou neutres.

Le 17 janvier 2017, l'administration Obama décide de commuer la peine de Manning, rendant possible sa libération avant le terme de sa condamnation initiale. Manning sort de prison le 17 mai 2017, sept ans après son arrestation survenue le 20 mai 2010.

_[Source : wikipedia](https://fr.wikipedia.org/wiki/Chelsea_Manning)- CC-By-SA _

\newpage

## Aaron Swartz

**Aaron Swartz**, né le 8 novembre 1986 à Chicago et mort le 11 janvier 2013 à New York1, est un informaticien, écrivain, militant politique et hacktiviste américain.

Fervent partisan de la liberté numérique, il consacra sa vie à la défense de la « culture libre », convaincu que l'accès à la connaissance est un moyen d'émancipation et de justice.

Aaron Swartz a eu une influence décisive dans l’essor de l’Internet. Il participa au développement de plusieurs techniques, notamment le format flux RSS ou encore celle des licences Creative Commons3 (CC). Sa contribution ne s'arrête pas au plan technique, il fut aussi connu pour ses efforts de démocratisation de l’information sur le web en manifestant contre des projets de loi tels que la Stop Online Piracy Act (SOPA).

Écrivain prolifique sous différentes formes (blogs, pamphlets politiques, textes de conférences), l'ouvrage Celui qui pourrait changer le monde (paru en français en 2017) rassemble ses principaux textes qui reflètent son engagement intellectuel sur des enjeux sociétaux dont le droit d'auteur, la liberté d'accès des connaissances et des savoirs dont les publications scientifiques ou la transparence en politique.

Il a étendu ses réflexions dans le domaine de la sociologie, l'éducation civique et politique.

Il se suicide le 11 janvier 2013 à l'âge de 26 ans dans son appartement. Son procès fédéral en lien avec des accusations de fraude électronique devait débuter le mois suivant.

_[Source : Wikipedia](https://fr.wikipedia.org/wiki/Aaron_Swartz)- CC-By-SA _

\newpage

## Julian Assange

**Julian Assange**, né le 3 juillet 1971 à Townsville, est un journaliste, informaticien et cybermilitant australien.

Il est surtout connu comme fondateur, rédacteur en chef et porte-parole de WikiLeaks. Devant la menace d'une extradition aux États-Unis, où il fait l'objet de poursuites judiciaires, il vit réfugié à l’ambassade d’Équateur à Londres entre 2012 et 2019. Le président équatorien, Lenín Moreno, le déchoit en avril 2019 de la nationalité équatorienne — obtenue l’année précédente — et met fin à son droit d'asile ; la police britannique est alors invitée à pénétrer dans l'ambassade équatorienne, où il est arrêté. Les États-Unis demandent immédiatement son extradition.

_[Source : wikipedia](https://fr.wikipedia.org/wiki/Julian_Assange)- CC-By-SA _

\newpage

## Edward Snowden

**Edward Joseph Snowden**, né le 21 juin 1983 à Elizabeth City, Caroline du Nord, est un lanceur d'alerte américain. Informaticien1, ancien employé de la Central Intelligence Agency (CIA) et de la National Security Agency (NSA), il a révélé les détails de plusieurs programmes de surveillance de masse américains et britanniques4.

À partir du 5 juin 2013, Snowden rend publiques par l’intermédiaire des médias, notamment The Guardian et The Washington Post, des informations classées top-secrètes de la NSA concernant la captation des métadonnées des appels téléphoniques aux États-Unis, ainsi que les systèmes d’écoute sur internet des programmes de surveillance PRISM, XKeyscore, Boundless Informant et Bullrun du gouvernement américain et les programmes de surveillance Tempora, Muscular et Optic Nerve du gouvernement britannique. Pour justifier ses révélations, il a indiqué que son « seul objectif est de dire au public ce qui est fait en son nom et ce qui est fait contre lui. ».

À la suite de ses révélations, Edward Snowden est inculpé le 22 juin 2013 par le gouvernement américain sous les chefs d’accusation d’espionnage, vol et utilisation illégale de biens gouvernementaux.

S'exilant à Hong Kong en juin 2013 puis à Moscou, Edward Snowden obtient le 31 juillet 2013 l’asile temporaire en Russie. Le 1er août 2014 il obtient un droit de résidence pour trois ans en Russie.

Le 14 avril 2014, l’édition américaine du Guardian et le Washington Post se voient décerner le prix Pulitzer pour la publication des révélations sur le système de surveillance de la NSA, rendues possibles grâce aux documents fournis par Snowden.

Le 18 janvier 2017, la Russie prolonge son droit d'asile de trois ans (jusqu'en 2020). 

_[Source : wikipedia](https://fr.wikipedia.org/wiki/Edward_Snowden)- CC-By-SA _
